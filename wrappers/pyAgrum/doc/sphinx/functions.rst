Functions from pyAgrum
======================

Useful functions in pyAgrum

.. autofunction:: pyAgrum.about

.. autofunction:: pyAgrum.fastBN

.. autofunction:: pyAgrum.getPosterior

Input/Output for bayesian networks
----------------------------------

.. autofunction:: pyAgrum.availableBNExts

.. autofunction:: pyAgrum.loadBN

.. autofunction:: pyAgrum.saveBN

Input for influence diagram
----------------------------------
.. autofunction:: pyAgrum.loadID
