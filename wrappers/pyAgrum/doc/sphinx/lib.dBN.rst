Module dynamic bayesian network
===============================

.. image:: _static/dBN.png

.. automodule:: pyAgrum.lib.dynamicBN
    :members:
    :undoc-members:
    :show-inheritance:
    :noindex:
