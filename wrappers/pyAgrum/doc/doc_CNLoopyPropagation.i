%feature("docstring") gum::credal::CNLoopyPropagation
"
Class used for inferences in credal networks with Loopy Propagation algorithm.

Available constructors:

    ``CNLoopyPropagation(cn) -> CNLoopyPropagation``

Parameters
----------
cn: pyAgrum.CredalNet
  a Credal network
"