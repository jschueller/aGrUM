%feature("docstring") gum::LoopySamplingInference<double,gum::GibbsSampling>
"
Class used for inferences using a loopy version of Gibbs sampling.

Available constructors:
	``LoopyGibbsSampling(bn) -> LoopyGibbsSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"