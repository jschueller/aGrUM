%feature("docstring") ggum::LoopySamplingInference<double,gum::MonteCarloSampling>
"
Class used for inferences using a loopy version of Monte Carlo sampling.

Available constructors:
	``LoopyMonteCarloSampling(bn) -> LoopyMonteCarloSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"