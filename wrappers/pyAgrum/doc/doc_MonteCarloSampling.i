%feature("docstring") gum::MonteCarloSampling
"
Class used for Monte Carlo sampling inference algorithm.

Available constructors:
	``MonteCarloSampling(bn) -> MonteCarloSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"