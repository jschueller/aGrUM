%feature("docstring") gum::LoopyBeliefPropagation
"
Class used for inferences using loopy belief propagation algorithm.

Available constructors:
	``LoopyBeliefPropagation(bn) -> LoopyBeliefPropagation``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"