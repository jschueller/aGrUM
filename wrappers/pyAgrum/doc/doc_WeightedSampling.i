%feature("docstring") gum::WeightedSampling
"
Class used for Weighted sampling inference algorithm.

Available constructors:
	``WeightedSampling(bn) -> WeightedSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"