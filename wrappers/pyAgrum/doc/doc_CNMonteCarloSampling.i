%feature("docstring") gum::credal::CNMonteCarloSampling
"
Class used for inferences in credal networks with Monte Carlo sampling algorithm.

Available constructors:

    ``CNMonteCarloSampling(cn) -> CNMonteCarloSampling``

Parameters
----------
cn: pyAgrum.CredalNet
  a Credal network
"