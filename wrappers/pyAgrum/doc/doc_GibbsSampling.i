%feature("docstring") gum::GibbsSampling
"
Class for making Gibbs sampling inference in bayesian networks.

Available constructors:
	``GibbsSampling(bn) -> GibbsSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"