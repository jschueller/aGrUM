%feature("docstring") gum::LazyPropagation
"
Class used for Lazy Propagation

Available constructors:
	``LazyPropagation(bn) -> LazyPropagation``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"