%feature("docstring") gum::ImportanceSampling
"
Class used for inferences using the Importance Sampling algorithm.

Available constructors:
	``ImportanceSampling(bn) -> ImportanceSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"