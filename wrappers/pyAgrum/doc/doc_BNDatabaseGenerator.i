%feature("docstring") BNDatabaseGenerator
"
BNGenerator is used to easily generate databases from a gum.BayesNet.

Available constructors:

    ``BNDatabaseGenerator(bn) -> BNDatabaseGenerator``

Parameters
----------
bn: gum.BayesNet
    the Bayesian network used to generate data.
"
