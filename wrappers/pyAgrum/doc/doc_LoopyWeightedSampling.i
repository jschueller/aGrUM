%feature("docstring") gum::LoopySamplingInference<double,gum::WeightedSampling>
"
Class used for inferences using a loopy version of weighted sampling.

Available constructors:
	``LoopyWeightedSampling(bn) -> LoopyWeightedSampling``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"