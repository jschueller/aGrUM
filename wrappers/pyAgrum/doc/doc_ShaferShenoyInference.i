%feature("docstring") gum::ShaferShenoyInference
"
Class used for Shafer-Shenoy inferences.

Available constructors:
	``ShaferShenoyInference(bn) -> ShaferShenoyInference``

Parameters
----------
bn : pyAgrum.BayesNet
	a Bayesian network
"