/***************************************************************************
 *   Copyright (C) 2005 by Christophe GONZALES and Pierre-Henri WUILLEMIN  *
 *   {prenom.nom}_at_lip6.fr                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/**
 * @file
 * @brief Classes of signaler.
 *
 * For more documentation, @see signaler0.h
 *
 * @author Pierre-Henri WUILLEMIN and Christophe GONZALES
 *
 */
#ifndef GUM_SIGNALER_H
#define GUM_SIGNALER_H
#include <functional>

#include <agrum/core/signal/listener.h>

#include <agrum/core/signal/signaler0.h>
#include <agrum/core/signal/signaler1.h>
#include <agrum/core/signal/signaler2.h>
#include <agrum/core/signal/signaler3.h>
#include <agrum/core/signal/signaler4.h>
#include <agrum/core/signal/signaler5.h>
#include <agrum/core/signal/signaler6.h>
#include <agrum/core/signal/signaler7.h>

#endif   // GUM_SIGNALER_H
